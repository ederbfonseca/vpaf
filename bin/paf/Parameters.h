// Copyright 2018 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

/* VERBOSE_PARSER turns on the output (on the standard output) of information when the input data are parsed. */
/* #define VERBOSE_PARSER */

/* DEBUG_GROW turns on the output (on the standard output) of information during the growing of the patterns.  This option may be enabled by who wishes to understand how a small number of patterns grow. */
/* #define DEBUG_GROW */

/* DEBUG_AGGLOMERATE turns on the output (on the standard output) of information during the agglomeration of the patterns.  This option may be enabled by who wishes to understand how a small number of patterns are agglomerated. */
/* #define DEBUG_AGGLOMERATE */

/* DEBUG_SELECT turns on the output (on the standard output) of information during the selection of the patterns.  This option may be enabled by who wishes to understand how a small number of patterns are selected. */
/* #define DEBUG_SELECT */

/* NUMERIC_PRECISION turns ou the output (on the standard output) of the maximal possible round-off error made when internally storing a membership degree for growing and agglomerating and, then, for selecting. */
/* #define NUMERIC_PRECISION */

/* DETAILED_TIME turns on the output (on the standard output) of a more detailed analysis of how the time is spent.  It gives (in this order): */
/* - the tensor parsing time */
/* - the tensor shifting time */
/* - the inital density maximization time */
/* - the inital sub-pattern discarding time */
/* - the further growing time */
/* - the agglomeration initialization time */
/* - the agglomeration time */
/* - the tensor reduction time */
/* - the selection time */
/* #define DETAILED_TIME */

/* TIME turns on the output (on the standard output) of the running time of paf. */
/* #define TIME */

/* GNUPLOT modifies the outputs of NUMERIC_PRECISION, DETAILED_TIME and TIME.  They become tab separated values.  The output order is: */
/* - tensor parsing time (#ifdef DETAILED_TIME) */
/* - numeric precision for growing and agglomerating (#ifdef NUMERIC_PRECISION) */
/* - tensor shifting time (#ifdef DETAILED_TIME) */
/* - inital density maximization time (#ifdef DETAILED_TIME) */
/* - inital sub-pattern discarding time (#ifdef DETAILED_TIME) */
/* - further growing time (#ifdef DETAILED_TIME) */
/* - agglomeration initialization time (#ifdef DETAILED_TIME) */
/* - agglomeration time (#ifdef DETAILED_TIME) */
/* - numeric precision for selecting (#ifdef NUMERIC_PRECISION) */
/* - tensor reduction time (#ifdef DETAILED_TIME) */
/* - selection time (#ifdef DETAILED_TIME) */
/* - total time (#ifdef TIME) */
/* #define GNUPLOT */

#endif /*PARAMETERS_H_*/
