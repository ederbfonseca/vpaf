// Copyright 2018,2019 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CANDIDATE_VARIABLE_H_
#define CANDIDATE_VARIABLE_H_

#include <set> 
#include <iomanip>

#include "AbstractRoughTensor.h"

class CandidateVariable
{
 public:
  CandidateVariable(const CandidateVariable& otherCandidateVariable);
  CandidateVariable(CandidateVariable&& otherCandidateVariable);
  CandidateVariable(vector<vector<unsigned int>>& nSet);

  bool operator<(const CandidateVariable& otherCandidateVariable) const;

  static void forwardSelect(const unsigned int nbOfCandidateVariablesHavingAllElements, AbstractRoughTensor* roughTensor, const bool isVerbose);
  tuple<vector<vector<unsigned int>>, vector<vector<unsigned int>>, unsigned int, unsigned int, unsigned int> makeUnionAndIntersection(const CandidateVariable& otherPattern) const;
  float quadraticErrorVariation(const CandidateVariable& otherPattern) const;
  static void distanceMatrixOutputFile(const string distanceMatrixOutputFileName);


 private:
  const vector<vector<unsigned int>> nSet;
  const int density;
  unsigned int area;
  long long lowerBound;		/* of the quadratic error variation; the exact variation if the pattern was selected */

  static AbstractRoughTensor* roughTensor;
  static TrieWithPrediction tensor;
  static multiset<CandidateVariable> candidates;
  static vector<CandidateVariable> selection;
  static long long quadraticError;
  static unsigned int nbOfTuples;

  friend ostream& operator<<(ostream& out, const CandidateVariable& pattern);

  long long getG() const;
  void select(const long long quadraticErrorVariation);
  long long computeLowerBoundAndGetQuadraticErrorVariation();
  void reselect() const;
  void output() const;
};

#endif	/* CANDIDATE_VARIABLE_H_ */
