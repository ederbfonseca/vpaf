// Copyright 2018,2019 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TRIE_WITH_PREDICTION_H_
#define TRIE_WITH_PREDICTION_H_

#include "TubeWithPrediction.h"

class TrieWithPrediction final : public AbstractDataWithPrediction
{
 public:
  TrieWithPrediction();
  TrieWithPrediction(TrieWithPrediction&& otherTrieWithPrediction);
  TrieWithPrediction(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd);
  TrieWithPrediction(vector<float>::const_iterator& membershipIt, const unsigned int unit, const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd);

  ~TrieWithPrediction();

  TrieWithPrediction& operator=(TrieWithPrediction&& otherTrieWithPrediction);

  void setTuple(const vector<unsigned int>::const_iterator idIt, const int membership);
  int sum(const vector<vector<unsigned int>>& nSet) const;
  int density(const vector<vector<unsigned int>>& nSet) const;
  void membershipSumOnSlice(const vector<vector<unsigned int>>::const_iterator dimensionIt, int& sum) const;
  long long initEstimationAndSumNegativeDensityMinus2Memberships(const vector<vector<unsigned int>>& tuples, const int density);
  void initEstimationAndSumNegativeDensityMinus2Memberships(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density, long long& sum);
  pair<unsigned int, long long> nullModel() const; /* to be called after constructing every candidate and before selecting any */
  void nullModel(unsigned int& nbOfCoveredTuples, long long& quadraticError) const;
  void addToModel(const vector<vector<unsigned int>>& tuples, const int density);
  void addToModel(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density);
  pair<long long, long long> lowerBoundAndQuadraticErrorVariation(const vector<vector<unsigned int>>& tuples, const int density) const;
  void lowerBoundAndQuadraticErrorVariation(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density, long long& lowerBound, long long& delta) const;
  pair<unsigned int, long long> resetNullModel();
  void resetNullModel(unsigned int& nbOfCoveredTuples, long long& quadraticError);
  void mark(const vector<vector<unsigned int>>& tuples);
  void mark(const vector<vector<unsigned int>>::const_iterator dimensionIt);

 private:
  vector<AbstractDataWithPrediction*> hyperplanes;
};

#endif /*TRIE_WITH_PREDICTION_H_*/
