// Copyright 2018,2019 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf.  If not, see <https://www.gnu.org/licenses/>.
#include <cmath> 
#include "CandidateVariable.h"
//#include "DendrogramNode.cpp"

AbstractRoughTensor* CandidateVariable::roughTensor;
TrieWithPrediction CandidateVariable::tensor;
multiset<CandidateVariable> CandidateVariable::candidates;
vector<CandidateVariable> CandidateVariable::selection;
long long CandidateVariable::quadraticError;
unsigned int CandidateVariable::nbOfTuples;

CandidateVariable::CandidateVariable(const CandidateVariable& otherCandidateVariable): nSet(otherCandidateVariable.nSet), density(otherCandidateVariable.density), area(otherCandidateVariable.area), lowerBound(otherCandidateVariable.lowerBound)
{
}

CandidateVariable::CandidateVariable(CandidateVariable&& otherCandidateVariable): nSet(std::move(otherCandidateVariable.nSet)), density(otherCandidateVariable.density),area(otherCandidateVariable.area), lowerBound(otherCandidateVariable.lowerBound)
{
}

CandidateVariable::CandidateVariable(vector<vector<unsigned int>>& nSetParam): nSet(std::move(nSetParam)), density(tensor.density(nSet)),area(1), lowerBound(density * tensor.initEstimationAndSumNegativeDensityMinus2Memberships(nSet, density))
{
  for (const vector<unsigned int>& dimension : nSet)
  {
    area *= dimension.size();
  }
}

bool CandidateVariable::operator<(const CandidateVariable& otherCandidateVariable) const
{
  return lowerBound < otherCandidateVariable.lowerBound;
}

ostream& operator<<(ostream& out, const CandidateVariable& pattern)
{
  CandidateVariable::roughTensor->printPattern(pattern.nSet, pattern.density, out);
  return out;
}

long long CandidateVariable::getG() const
{
  unsigned int area = 1;
  //area = 1;
  for (const vector<unsigned int>& dimension : nSet)
    {
      area *= dimension.size();
    }
  return static_cast<long long>(area) * density * density;
}

void CandidateVariable::select(const long long quadraticErrorVariation)
{
#ifdef DEBUG_SELECT
  cout << *this << " selected\nQuadratic error of current model: " << static_cast<double>(quadraticError + quadraticErrorVariation) / AbstractRoughTensor::getUnit() / AbstractRoughTensor::getUnit() << " (decreased by " << static_cast<double>(-quadraticErrorVariation) * 100 / quadraticError << "% > ";
#endif
  lowerBound = quadraticErrorVariation;
  quadraticError += quadraticErrorVariation;
  tensor.addToModel(nSet, density);
  selection.emplace_back(std::move(*this));
}

long long CandidateVariable::computeLowerBoundAndGetQuadraticErrorVariation()
{
  const pair<long long, long long> lowerBoundAndQuadraticErrorVariation = tensor.lowerBoundAndQuadraticErrorVariation(nSet, density);
  lowerBound = lowerBoundAndQuadraticErrorVariation.first;
  return lowerBoundAndQuadraticErrorVariation.second;
}

void CandidateVariable::reselect() const
{
#ifdef DEBUG_SELECT
  cout << *this << " reselected\nQuadratic error of current model: " << static_cast<double>(quadraticError + lowerBound) / AbstractRoughTensor::getUnit() / AbstractRoughTensor::getUnit() << " (decreased by " << static_cast<double>(-lowerBound) * 100 / quadraticError << "% > ";
#endif
  quadraticError += lowerBound;
  tensor.mark(nSet);
}

void CandidateVariable::output() const
{
  roughTensor->output(nSet, density);
}

void CandidateVariable::forwardSelect(const unsigned int nbOfCandidateVariablesHavingAllElements, AbstractRoughTensor* roughTensorParam, const bool isVerbose)
{
  // Set tensor
#ifdef DETAILED_TIME
  steady_clock::time_point startingPoint = steady_clock::now();
#endif
  if (isVerbose)
    {
      cout << "Reducing fuzzy tensor to elements in patterns ... " << flush;
    }
  roughTensor = roughTensorParam;
  tensor = roughTensor->projectTensor(nbOfCandidateVariablesHavingAllElements);
  if (isVerbose)
    {
      cout << "\rReducing fuzzy tensor to elements in patterns: done.\n";
    }
#ifdef DETAILED_TIME
#ifdef GNUPLOT
  cout << '\t' << duration_cast<duration<double>>(steady_clock::now() - startingPoint).count();
#else
  cout << "Tensor reduction time: " << duration_cast<duration<double>>(steady_clock::now() - startingPoint).count() << "s\n";
  startingPoint = steady_clock::now();
#endif
#endif
  if (isVerbose)
    {
      cout << "Selecting patterns ... " << flush;
    }
  // Construct the candidates and find the first pattern to select
  multiset<CandidateVariable>::const_iterator bestCandidateIt;
  long long max = 0;
  vector<vector<vector<unsigned int>>>& raw = AbstractRoughTensor::getCandidateVariables();
  for (vector<vector<unsigned int>>& pattern : raw)
    {
      const multiset<CandidateVariable>::const_iterator candidateIt = candidates.emplace(pattern);
      if (candidateIt->density > 0)
	{
	  const long long g = candidateIt->getG();
	  if (g > max)
	    {
	      max = g;
	      bestCandidateIt = candidateIt;
	    }
	}
      else
	{
	  candidates.erase(candidateIt);
	}
    }
  raw.clear();
  raw.shrink_to_fit();
  const pair<unsigned int, long long> nbOfCoveredTuplesAndQuadraticError = tensor.nullModel();
  nbOfTuples = nbOfCoveredTuplesAndQuadraticError.first;
  quadraticError = nbOfCoveredTuplesAndQuadraticError.second;
#ifdef DEBUG_SELECT
  cout << "Nb of tuples the candidates cover: " << nbOfTuples << "\nQuadratic error of null model on these tuples: " << static_cast<double>(quadraticError) / AbstractRoughTensor::getUnit() / AbstractRoughTensor::getUnit() << '\n';
#endif
  if (max > -quadraticError * (exp((-2 - static_cast<double>(12) / (nbOfTuples - 3) + static_cast<double>(4) / (nbOfTuples - 2)) / nbOfTuples) - 1))
    {
      CandidateVariable selectedPattern(*bestCandidateIt);
      candidates.erase(bestCandidateIt);
      selection.reserve(candidates.size());
      selectedPattern.select(-max);
#ifdef DEBUG_SELECT
      cout << 100 - 100 * exp((-2 - static_cast<double>(12) / (nbOfTuples - 3) + static_cast<double>(4) / (nbOfTuples - 2)) / nbOfTuples) << "%, the minimal acceptable)\n";
#endif
    }
  unsigned int nbOfParameters = 1;
  for (bool isNoPatternDecreasesAICc; ; )
    {
      isNoPatternDecreasesAICc = true;
      vector<CandidateVariable> updated;
      updated.reserve(candidates.size());
      vector<CandidateVariable>::iterator argminIt;
      long long min = quadraticError * (exp(2 * ((nbOfParameters + 1) * (static_cast<double>(nbOfParameters) / (nbOfTuples - nbOfParameters - 1) - static_cast<double>(nbOfParameters + 2) / (nbOfTuples - nbOfParameters - 2)) - 1) / nbOfTuples) - 1); // a larger quadratic error variation increases AICc
      while (!candidates.empty() && candidates.begin()->lowerBound < min)
	{
	  updated.emplace_back(std::move(*candidates.begin()));
	  candidates.erase(candidates.begin());
	  const long long quadraticErrorVariation = updated.back().computeLowerBoundAndGetQuadraticErrorVariation();
	  if (quadraticErrorVariation < min)
	    {
	      min = quadraticErrorVariation;
	      argminIt = --updated.end();
	      isNoPatternDecreasesAICc = false;
	    }
	}
      if (isNoPatternDecreasesAICc)
	{
	  break;
	}
      argminIt->select(min);
#ifdef DEBUG_SELECT
      cout << 100 - 100 * exp(2 * ((nbOfParameters + 1) * (static_cast<double>(nbOfParameters) / (nbOfTuples - nbOfParameters - 1) - static_cast<double>(nbOfParameters + 2) / (nbOfTuples - nbOfParameters - 2)) - 1) / nbOfTuples) << "%, the minimal acceptable)\n";
#endif
      for (vector<CandidateVariable>::iterator patternIt = updated.begin(); patternIt != argminIt; ++patternIt)
	{
	  candidates.emplace(std::move(*patternIt));
	}
      const vector<CandidateVariable>::iterator end = updated.end();
      while (++argminIt != end)
	{
	  candidates.emplace(std::move(*argminIt));
	}
      ++nbOfParameters;
    }
  // Refine the model considering the number of tuples selection covers, until a fixed point is reached
  const vector<CandidateVariable>::const_iterator begin = selection.begin();
  vector<CandidateVariable>::const_iterator end = selection.end();
  for (vector<CandidateVariable>::const_iterator patternIt = begin; ; patternIt = begin)
    {
      const pair<unsigned int, long long> nbOfCoveredTuplesAndQuadraticError = tensor.resetNullModel();
      nbOfTuples = nbOfCoveredTuplesAndQuadraticError.first;
      quadraticError = nbOfCoveredTuplesAndQuadraticError.second;
#ifdef DEBUG_SELECT
      cout << end - begin << " patterns in selection\nNb of tuples selection covers: " << nbOfTuples << "\nQuadratic error of null model on these tuples: " << static_cast<double>(quadraticError) / AbstractRoughTensor::getUnit() / AbstractRoughTensor::getUnit() << "\nReconsidering selection\n";
#endif
      for (nbOfParameters = 1; patternIt != end && patternIt->lowerBound < quadraticError * (exp(2 * ((nbOfParameters + 1) * (static_cast<double>(nbOfParameters) / (nbOfTuples - nbOfParameters - 1) - static_cast<double>(nbOfParameters + 2) / (nbOfTuples - nbOfParameters - 2)) - 1) / nbOfTuples) - 1); ++nbOfParameters)
	{
	  patternIt->reselect();
#ifdef DEBUG_SELECT
	  cout << 100 - 100 * exp(2 * ((nbOfParameters + 1) * (static_cast<double>(nbOfParameters) / (nbOfTuples - nbOfParameters - 1) - static_cast<double>(nbOfParameters + 2) / (nbOfTuples - nbOfParameters - 2)) - 1) / nbOfTuples) << "%, the minimal acceptable)\n";
#endif
	  ++patternIt;
	}
      if (patternIt == end)
	{
	  break;
	}
      end = patternIt;
    }
#ifdef DEBUG_SELECT
  cout << "All patterns reselected: fixed point reached\n";
#endif
  //if (printMatrix)
  // {
     ofstream ofs;
     ofs.open("matrix.txt", ofstream::out | ofstream::trunc);
   
  for (vector<CandidateVariable>::const_iterator patternIt = begin; patternIt != end; ++patternIt)
    {
      for (vector<CandidateVariable>::const_iterator otherPatternIt = begin; otherPatternIt != patternIt; ++otherPatternIt)
      {
        ofs << setprecision(3) << patternIt->quadraticErrorVariation(*otherPatternIt) << " ";
        //cout << "\rGenerating similarity matrix" << end - begin << flush;
      }
      ofs << "0\n";
      patternIt->output();
    }
    ofs.close();
  if (isVerbose)
    {
      cout << "\rSelecting patterns: " << end - begin << " patterns selected.\n";
    }
#ifdef DETAILED_TIME
#ifdef GNUPLOT
  cout << '\t' << duration_cast<duration<double>>(steady_clock::now() - startingPoint).count();
#else
  cout << "Selection time: " << duration_cast<duration<double>>(steady_clock::now() - startingPoint).count() << "s\n";
#endif
#endif
}

tuple<vector<vector<unsigned int>>, vector<vector<unsigned int>>, unsigned int, unsigned int, unsigned int> CandidateVariable::makeUnionAndIntersection(const CandidateVariable& otherPattern) const
{
  vector<vector<unsigned int>> unionNSet;
  unionNSet.reserve(nSet.size());

  vector<vector<unsigned int>> intersection;
  intersection.reserve(nSet.size());
  unsigned int intersectionArea = 1;

  unsigned int unionArea = 1;
  
  unsigned int productSumDimension = 1;

  vector<vector<unsigned int>>::const_iterator otherNSetIt = otherPattern.nSet.begin();
  for (const vector<unsigned int>& nSetDimension : nSet)
  {
    vector<unsigned int> unionNSetDimension;
    unionNSetDimension.reserve(nSetDimension.size() + otherNSetIt->size());
    set_union(nSetDimension.begin(), nSetDimension.end(), otherNSetIt->begin(), otherNSetIt->end(), back_inserter(unionNSetDimension));
    unionNSet.emplace_back(unionNSetDimension);

    vector<unsigned int> intersectionDimension;
    intersectionDimension.reserve(nSetDimension.size());
    set_intersection(nSetDimension.begin(), nSetDimension.end(), otherNSetIt->begin(), otherNSetIt->end(), back_inserter(intersectionDimension));
    intersectionArea *= intersectionDimension.size();
    intersection.emplace_back(intersectionDimension);
    
    unionArea *= unionNSetDimension.size();
    
    productSumDimension *= (nSetDimension.size() + otherNSetIt->size());

    ++otherNSetIt;
  }

  return make_tuple(unionNSet, intersection, intersectionArea, unionArea, productSumDimension);

}


float CandidateVariable::quadraticErrorVariation(const CandidateVariable& otherPattern) const
{
  auto unionInterArea = makeUnionAndIntersection(otherPattern);
  /*
  cout << "|X| " << area << "\n";
  cout << "|Y| " << otherPattern.area << "\n";
  cout << "|XUY| " << get<3>(unionInterArea) << "\n";
  cout << "|X INTER Y| " << get<2>(unionInterArea) << "\n";

  cout << "d(X) " << density << "\n";
  cout << "d(Y) " << otherPattern.density << "\n";
  cout << "d(XUY) " << tensor.density(get<0>(unionInterArea)) << "\n";
  cout << "d(|X INTER Y|) " << tensor.sum(get<1>(unionInterArea))<< "\n";
  
  cout << "Product (|Xi| + |Yi|) " << get<4>(unionInterArea) << "\n";
  */
  
  int dxuy = tensor.density(get<0>(unionInterArea));
  
  
  float d = (area * density + otherPattern.area * otherPattern.density) / get<4>(unionInterArea);
  
  float d_ = (d - density) * (area * (2 * 0 + d + density) - 2 * area * density) + (d - otherPattern.density) * (area * (2 * 0 + d + otherPattern.density) - 2 * otherPattern.area * otherPattern.density);
  
  
  d_ = abs(d_);
  //d_ = AbstractRoughTensor::getUnit() * AbstractRoughTensor::getUnit();
  //cout << d_ << " ";
  //d_ = area * (d - density) + otherPattern.area * (d - otherPattern.density) + 2 * (area * density) * (density - d) + 2 * (otherPattern.area * otherPattern.density) * (otherPattern.density - d);
    
  
  //cout << "d_ " << d_ << "\n";
  
  if (density <= dxuy)
  {
    if (otherPattern.density <= dxuy)
    {
      return 0;
    }
    unsigned long long densityDiff2 = otherPattern.density - dxuy;
    return (densityDiff2 * densityDiff2 * otherPattern.area) / d_;
  }
  unsigned long long densityDiff1 = density - dxuy;
  if (otherPattern.density <= dxuy)
    {
      return (densityDiff1 * densityDiff1 * area) / d_;
    }
  unsigned long long densityDiff2 = otherPattern.density - dxuy;
  if (density < otherPattern.density)
  {
    return ((densityDiff1 * densityDiff1 * area) + (densityDiff2 * densityDiff2 * otherPattern.area) + get<3>(unionInterArea) * (density * density - dxuy * dxuy) - 2 * (density - dxuy) * get<2>(unionInterArea) * tensor.sum(get<1>(unionInterArea))) / d_;
  }
  return ((densityDiff1 * densityDiff1 * area) + (densityDiff2 * densityDiff2 * otherPattern.area) + get<3>(unionInterArea) * (otherPattern.density * otherPattern.density - dxuy * dxuy) - 2 * (otherPattern.density - dxuy) * get<2>(unionInterArea) * tensor.sum(get<1>(unionInterArea))) / d_;
}
/*
void CandidateVariable::distanceMatrixOutputFile(const string distanceMatrixOutputFileName)
{
  ofstream ofs;
  ofs.open(distanceMatrixOutputFileName, ofstream::out | ofstream::app);
  const vector<CandidateVariable>::const_iterator begin = selection.begin();
  vector<CandidateVariable>::const_iterator end = selection.end();
  for (vector<CandidateVariable>::const_iterator patternIt = begin; patternIt != end; ++patternIt)
  {
    ofs << "0 ";
    for (vector<CandidateVariable>::const_iterator otherPatternIt = begin; otherPatternIt != patternIt; ++otherPatternIt)
    {
      ofs << patternIt->quadraticErrorVariation(*otherPatternIt) << " ";
    }
    ofs << "\n";
  }
  ofs.close();

}
*/
