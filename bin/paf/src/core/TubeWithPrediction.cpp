// Copyright 2018,2019 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf.  If not, see <https://www.gnu.org/licenses/>.

#include "TubeWithPrediction.h"

int TubeWithPrediction::defaultMembership;

TubeWithPrediction::TubeWithPrediction(const unsigned int size): tube(size, pair<int, int>(-1, defaultMembership))
{
}

TubeWithPrediction::TubeWithPrediction(vector<float>::const_iterator& membershipIt, const unsigned int size, const unsigned int unit): tube()
{
  tube.reserve(size);
  for (const vector<float>::const_iterator tubeEnd = membershipIt + size; membershipIt != tubeEnd; ++membershipIt)
    {
      tube.emplace_back(-1, unit * *membershipIt);
    }
}

void TubeWithPrediction::setTuple(const vector<unsigned int>::const_iterator idIt, const int membership)
{
  tube[*idIt].second = membership;
}

void TubeWithPrediction::membershipSumOnSlice(const vector<vector<unsigned int>>::const_iterator dimensionIt, int& sum) const
{
  for (const unsigned int id : *dimensionIt)
    {
      sum += tube[id].second;
    }
}

void TubeWithPrediction::initEstimationAndSumNegativeDensityMinus2Memberships(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density, long long& sum)
{
  for (const unsigned int id : *dimensionIt)
    {
      pair<int, int>& estimatedAndRealMembership = tube[id];
      estimatedAndRealMembership.first = 0;
      const int term = density - 2 * estimatedAndRealMembership.second;
      if (term < 0)
	{
	  sum += term;
	}
    }
}

void TubeWithPrediction::nullModel(unsigned int& nbOfCoveredTuples, long long& quadraticError) const
{
  for (const pair<int, int>& estimatedAndRealMembership : tube)
    {
      if (estimatedAndRealMembership.first == 0)
	{
	  quadraticError += static_cast<long long>(estimatedAndRealMembership.second) * estimatedAndRealMembership.second;
	  ++nbOfCoveredTuples;
	}
    }
}

void TubeWithPrediction::addToModel(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density)
{
  for (const unsigned int id : *dimensionIt)
    {
      int& estimatedMembership = tube[id].first;
      if (estimatedMembership < density)
	{
	  estimatedMembership = density;
	}
    }
}

void TubeWithPrediction::lowerBoundAndQuadraticErrorVariation(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density, long long& lowerBound, long long& delta) const
{
  for (const unsigned int id : *dimensionIt)
    {
      const pair<int, int>& estimatedAndRealMembership = tube[id];
      if (estimatedAndRealMembership.first < density)
	{
	  const long long term = static_cast<long long>(density - estimatedAndRealMembership.second) * (density - estimatedAndRealMembership.second) - static_cast<long long>(estimatedAndRealMembership.first - estimatedAndRealMembership.second) * (estimatedAndRealMembership.first - estimatedAndRealMembership.second);
	  if (term < 0)
	    {
	      lowerBound += term;
	    }
	  delta += term;
	}
    }
}

void TubeWithPrediction::resetNullModel(unsigned int& nbOfCoveredTuples, long long& quadraticError)
{
  for (pair<int, int>& estimatedAndRealMembership : tube)
    {
      if (estimatedAndRealMembership.first > 0)
	{
	  estimatedAndRealMembership.first = 0;
	  quadraticError += static_cast<long long>(estimatedAndRealMembership.second) * estimatedAndRealMembership.second;
	  ++nbOfCoveredTuples;
	}
    }
}

void TubeWithPrediction::mark(const vector<vector<unsigned int>>::const_iterator dimensionIt)
{
  for (const unsigned int id : *dimensionIt)
    {
      tube[id].first = 1;
    }
}

void TubeWithPrediction::setDefaultMembership(const int defaultMembershipParam)
{
  defaultMembership = defaultMembershipParam;
}
