// Copyright 2018,2019 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf.  If not, see <https://www.gnu.org/licenses/>.

#include "TrieWithPrediction.h"

TrieWithPrediction::TrieWithPrediction(): hyperplanes()
{
}

TrieWithPrediction::TrieWithPrediction(TrieWithPrediction&& otherTrieWithPrediction): hyperplanes(std::move(otherTrieWithPrediction.hyperplanes))
{
}

TrieWithPrediction::TrieWithPrediction(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd): hyperplanes()
{
  const unsigned int cardinality = *cardinalityIt;
  hyperplanes.reserve(cardinality);
  const vector<unsigned int>::const_iterator nextCardinalityIt = cardinalityIt + 1;
  if (nextCardinalityIt + 1 == cardinalityEnd)
    {
      for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
	{
	  hyperplanes.push_back(new TubeWithPrediction(*nextCardinalityIt));
	}
      return;
    }
  for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
    {
      hyperplanes.push_back(new TrieWithPrediction(nextCardinalityIt, cardinalityEnd));
    }
}

TrieWithPrediction::TrieWithPrediction(vector<float>::const_iterator& membershipIt, const unsigned int unit, const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd): hyperplanes()
{
  const unsigned int cardinality = *cardinalityIt;
  hyperplanes.reserve(cardinality);
  const vector<unsigned int>::const_iterator nextCardinalityIt = cardinalityIt + 1;
  if (nextCardinalityIt + 1 == cardinalityEnd)
    {
      for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
	{
	  hyperplanes.push_back(new TubeWithPrediction(membershipIt, *nextCardinalityIt, unit));
	}
      return;
    }
  for (unsigned int hyperplaneId = 0; hyperplaneId != cardinality; ++hyperplaneId)
    {
      hyperplanes.push_back(new TrieWithPrediction(membershipIt, unit, nextCardinalityIt, cardinalityEnd));
    }
}

TrieWithPrediction::~TrieWithPrediction()
{
  for (AbstractDataWithPrediction* hyperplane : hyperplanes)
    {
      delete hyperplane;
    }
}

TrieWithPrediction& TrieWithPrediction::operator=(TrieWithPrediction&& otherTrieWithPrediction)
{
  hyperplanes = std::move(otherTrieWithPrediction.hyperplanes);
  return *this;
}

void TrieWithPrediction::setTuple(const vector<unsigned int>::const_iterator idIt, const int membership)
{
  hyperplanes[*idIt]->setTuple(idIt + 1, membership);
}

void TrieWithPrediction::membershipSumOnSlice(const vector<vector<unsigned int>>::const_iterator dimensionIt, int& sum) const
{
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      hyperplanes[id]->membershipSumOnSlice(nextDimensionIt, sum);
    }
}
int TrieWithPrediction::sum(const vector<vector<unsigned int>>& nSet) const
{
  long long sum = 0;
  const vector<vector<unsigned int>>::const_iterator secondDimensionIt = ++nSet.begin();
  for (const unsigned int id : nSet.front())
    {
      int sumOnSlice = 0;
      hyperplanes[id]->membershipSumOnSlice(secondDimensionIt, sumOnSlice);
      sum += sumOnSlice;
    }
  return sum;
}
int TrieWithPrediction::density(const vector<vector<unsigned int>>& nSet) const
{
  long long sum = 0;
  const vector<vector<unsigned int>>::const_iterator secondDimensionIt = ++nSet.begin();
  for (const unsigned int id : nSet.front())
    {
      int sumOnSlice = 0;
      hyperplanes[id]->membershipSumOnSlice(secondDimensionIt, sumOnSlice);
      sum += sumOnSlice;
    }
  for (const vector<unsigned int>& dimension : nSet)
    {
      sum /= dimension.size();
    }
  return sum;
}

long long TrieWithPrediction::initEstimationAndSumNegativeDensityMinus2Memberships(const vector<vector<unsigned int>>& tuples, const int density)
{
  long long sum = 0;
  initEstimationAndSumNegativeDensityMinus2Memberships(tuples.begin(), density, sum);
  return sum;
}

void TrieWithPrediction::initEstimationAndSumNegativeDensityMinus2Memberships(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density, long long& sum)
{
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      hyperplanes[id]->initEstimationAndSumNegativeDensityMinus2Memberships(nextDimensionIt, density, sum);
    }
}

pair<unsigned int, long long> TrieWithPrediction::nullModel() const
{
  pair<unsigned int, long long> nbOfCoveredTuplesAndQuadraticError(0, 0);
  nullModel(nbOfCoveredTuplesAndQuadraticError.first, nbOfCoveredTuplesAndQuadraticError.second);
  return nbOfCoveredTuplesAndQuadraticError;
}

void TrieWithPrediction::nullModel(unsigned int& nbOfCoveredTuples, long long& quadraticError) const
{
  for (const AbstractDataWithPrediction* hyperplane : hyperplanes)
    {
      hyperplane->nullModel(nbOfCoveredTuples, quadraticError);
    }
}

void TrieWithPrediction::addToModel(const vector<vector<unsigned int>>& tuples, const int density)
{
  addToModel(tuples.begin(), density);
}

void TrieWithPrediction::addToModel(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density)
{
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      hyperplanes[id]->addToModel(nextDimensionIt, density);
    }
}

pair<long long, long long> TrieWithPrediction::lowerBoundAndQuadraticErrorVariation(const vector<vector<unsigned int>>& tuples, const int density) const
{
  pair<long long, long long> lowerBoundAndDelta(0, 0);
  lowerBoundAndQuadraticErrorVariation(tuples.begin(), density, lowerBoundAndDelta.first, lowerBoundAndDelta.second);
  return lowerBoundAndDelta;
}

void TrieWithPrediction::lowerBoundAndQuadraticErrorVariation(const vector<vector<unsigned int>>::const_iterator dimensionIt, const int density, long long& lowerBound, long long& delta) const
{
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      hyperplanes[id]->lowerBoundAndQuadraticErrorVariation(nextDimensionIt, density, lowerBound, delta);
    }
}

pair<unsigned int, long long> TrieWithPrediction::resetNullModel()
{
  pair<unsigned int, long long> nbOfCoveredTuplesAndQuadraticError(0, 0);
  resetNullModel(nbOfCoveredTuplesAndQuadraticError.first, nbOfCoveredTuplesAndQuadraticError.second);
  return nbOfCoveredTuplesAndQuadraticError;
}

void TrieWithPrediction::resetNullModel(unsigned int& nbOfCoveredTuples, long long& quadraticError)
{
  for (AbstractDataWithPrediction* hyperplane : hyperplanes)
    {
      hyperplane->resetNullModel(nbOfCoveredTuples, quadraticError);
    }
}

void TrieWithPrediction::mark(const vector<vector<unsigned int>>& tuples)
{
  mark(tuples.begin());
}

void TrieWithPrediction::mark(const vector<vector<unsigned int>>::const_iterator dimensionIt)
{
  const vector<vector<unsigned int>>::const_iterator nextDimensionIt = dimensionIt + 1;
  for (const unsigned int id : *dimensionIt)
    {
      hyperplanes[id]->mark(nextDimensionIt);
    }
}

