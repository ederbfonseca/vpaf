import pandas as pd
import sys

# patterns recebe o nome do arquivo de saida de PAF
# o arquivo devera conter as colunas conforme abaixo:
# <dim_1> <dim_2> ... <dim_n densidade area

#uma saida nesse padrao pode ser obtida atraves do
#comando, por exemplo:
#./multidupehack -e '2 1 4' -s '4 8 2' retweets.fuzzy | ./paf --pa -vf retweets.fuzzy -a 10000 -o retweets.paf

#patterns = "retweets.paf"

patterns = sys.argv[1]
file_tree = sys.argv[2]
file_vis = sys.argv[3]

# importa-se tudo para um data frame
df = pd.read_csv(patterns , sep=' ', header=None)

# Vamos montar o DAG (Directed acyclic graph)
# transforma todos os dados em um array
# usa-se 'df.shape[1] - 2' porque nao ha
# necessidade de fazer calculos com as colunas
# densidade e area
for i in range(df.shape[1] - 2):
    df[i]= df[i].str.split(",") 

# Encontrando as arestas
# Dados dois padroes A e B, existe uma aresta
# (A,B) se A contem o padrao B e existe uma aresta
# (B,A) se B contem o padrao A
#print "total: ", df.shape[0]
edges = []
for i in range(df.shape[0]-1):
    for line in range(i+1, df.shape[0]):
        percent = 100*i/df.shape[0]
        sys.stdout.write("\rCalculating edges %.0f%%" % percent)
        sys.stdout.flush()
        col = 0
        left_contains = True
        while (col < (df.shape[1] -2)) & (left_contains):
            # result eh um booleano que eh True caso df[col][i] contenha
            # df[col][line]
            result = all(elem in df[col][i]  for elem in df[col][line])
            if result:
                col+=1
            else:
                left_contains = False
        if left_contains:
            # adiciona a aresta ao vector edges
            edges.append((i, line))
        # se o padrao A nao contem o padrao B
        # entao deve-se testar se o padrao B
        # contem o padrao A.
        else:
            col = 0
            right_contains = True
            while (col < (df.shape[1] -2)) & (right_contains):
                #print "line", line, "i", i, "col", col
                result = all(elem in df[col][line]  for elem in df[col][i])
                if result:
                    col+=1
                else:
                    right_contains = False
            if right_contains:
                edges.append((line, i))
sys.stdout.write("\rCalculating edges. done ")
sys.stdout.write("\n")


#import random
#import pprint

class Graph:
    nodes = []
    edges = []
    removed_edges = []
    def remove_edge(self,x,y):
        e = (x,y)
        try:
            self.edges.remove(e)
            #print("Removed edge %s" % str(e))
            self.removed_edges.append(e)
        except:
            #print("Attempted to remove edge %s, but it wasn't there" % str(e))
            pass

    def Nodes(self):
        return self.nodes

    # Sample data
    def __init__(self):
        self.nodes = list(range(df.shape[0]))
        self.edges = edges
sys.stdout.write("\rCalculating edges. %s edges found." % len(edges))
sys.stdout.write("\n")

G = Graph()
N = G.Nodes()

for x in N:
   for y in N:
      for z in N:
         #print("(%d,%d,%d)" % (x,y,z))
         if (x,y) != (y,z) and (x,y) != (x,z):
            if (x,y) in G.edges and (y,z) in G.edges:
                G.remove_edge(x,z)
   percent = 100*x/len(G.Nodes())
   sys.stdout.write("\rTransform DAG in Tree %.0f%%" % percent)
   sys.stdout.flush()
with open(file_tree, 'w') as fp:
    fp.write('\n'.join('%s %s' % x for x in G.edges))
fp.close()
sys.stdout.write("\rTransform DAG in Tree. done")
sys.stdout.write("\n")

'''
#We read the existing text from file in READ mode
src=open(patterns,"r")
fline="Pattern:Area\n"    #Prepending string
oline=src.readlines()
#Here, we prepend the string we want to on first line
oline.insert(0,fline)
src.close()
 
 
#We again open the file in WRITE mode 
src=open(patterns,"w")
src.writelines(oline)
src.close()
'''

#f = open("../vpaf/frontend/total.vis","w")
f = open(file_vis,"w")
header = "Id\tPattern\tDensity\tArea\n"
f.write(header)
#print tree

def printPattern(pattern):
    p = []
    #print pattern
    area = 1
    for x in range(len(pattern)):
        if (x < (len(pattern)-1)):
            patternAsVector = pattern[x].split(",")
            area = area * len(patternAsVector)
            patternAsVector = sorted(patternAsVector, key=lambda v: (v.upper(), v[0].islower()))
            #print ','.join(patternAsVector)
            #p.append(pattern[x])
            p.append(','.join(patternAsVector))
        else:
            density = pattern[x]
        #print area
    s = (' '.join(str(v) for v in p))
    return s,density,area


for i in range(df.shape[1] - 2):
    df[i] = df[i].str.join(",")
    #df[i]= df[i].str.split(",") 
    
if (len(G.edges) > 0):
    G.edges = sorted(G.edges, key=lambda tup: tup[1])
    child = G.edges.pop(0)

for i in range(df.shape[0]):
    s, d, a = printPattern(df.iloc[i])
    f.write(str(i)+ "\t" + s + "\t" + str(d) + "\t" + str(a) + "\n")
f.close()
