<?php
function parse_size($size) {
  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
  if ($unit) {
    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
  }
  else {
    return round($size);
  }
}
function confereArquivo($dir, $f){
	//$f = "files/".$f;
	$row = 1;
	if (($handle = fopen($dir.$f, "r")) !== FALSE) {
		$i = 0;
		echo "<h5>File sample:</h5>";
		while (($data = fgetcsv($handle, 0, " ")) !== FALSE) {
			//print_r($data);
			//echo "<br>";
			//$line = explode(" ", $data);
			//echo "data";
			//print_r($data);
			$fields = count($data);
			$row++;
			if ($i <= 5){
				$str = implode(" ",$data);
				echo $str."<br>";
			}
			$i++;
			//for ($c=0; $c < $fields; $c++) {
			//	echo $data[$c] . "<br />\n";
			//}
		}
	echo "(...)";
	echo "<hr />";
	//echo "fields ".$fields;
	/*
	echo '
	<div class="row">
          <div class="col-md-6 mb-4"><p>Fields: </p><h4>'.($fields -1).'</h4></div>
          <div class="col-md-6 mb-4"><p>Rows: </p><h4>'.$row.'</h4></div>
   </div>
	
	';
	*/
	fclose($handle);
	}
	//echo "<form action=\"../paf_exec.php\" enctype=\"multipart/form-data\" method=\"post\" id=\"params\">";
	echo '<label>Size of pattern:</label>';
	echo '<div class="row ">';
	for ($i=0; $i < ($fields-1);$i++){
		echo "<div class=\"col-md-auto\">";
		echo "<input name=\"dim".$i."\" style=\"max-width: 120px\" id=\"dim".$i."\" class=\"form-control input-sm\" type=\"text\" placeholder=\"Dimension ".$i."\" />";
		echo "</div>";
	}
	echo '</div>';
	echo '<div>';
	echo '<label for="noise">Noise [0-1]</label>';
	
	echo "<input name=\"noise\" style=\"max-width: 120px\" id=\"noise\" class=\"form-control input-sm\" type=\"text\" placeholder=\"Noise\" />";
	//echo '<label for="memory">Memory [GB]</label>';
	//echo "<input name=\"memory\" style=\"max-width: 120px\" id=\"memory\" class=\"form-control input-sm\" type=\"text\" placeholder=\"Memory \" />";
	//echo "<div>Coeficiente de selecao:</div>";
	//echo "<input name=\"coefficient\" style=\"max-width: 100px\" id=\"coefficient\" class=\"form-control input-sm\" type=\"text\" placeholder=\"Coeficiente\" />";
	echo '</div>';
	echo "<input name=\"dir\" id=\"dir\" type=\"hidden\" value=\"".$dir."\" />";
	echo "<input name=\"filename\" id=\"filename\" type=\"hidden\" value=\"".$f."\" />";
	//echo "<div class=\"col-md-8\"></div><div class=\"col-md-4\"><input type=\"button\" value=\"Executar\" onclick=\"executeFile();\" /></div>";
	echo "<div class=\"col-md-8\"></div><div class=\"col-md-4\"><input class=\"btn btn-primary mb-2\" type=\"submit\" value=\"Run PAF\"/></div>";
	//echo "</form>";
}
if ($_POST["label"]) {
    $label = $_POST["label"];
}
$dir = "files/".uniqid()."/";
mkdir($dir, 0777, true);

$temp = explode(".", $_FILES["file"]["name"]);
//echo "size ".$_FILES["file"]["size"];
$post_max_size = parse_size(ini_get('post_max_size'));
$upload_max = parse_size(ini_get('upload_max_filesize'));

if ($_FILES["file"]["size"] < $post_max_size && $_FILES["file"]["size"] < $upload_max) {
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    } else {
        $filename = $label.$_FILES["file"]["name"];
			move_uploaded_file($_FILES["file"]["tmp_name"],
           $dir . $filename);
         confereArquivo($dir, $filename);
    }

} else {
    echo "Invalid file";
    echo "File Size: ".$_FILES["file"]["size"];
    echo "Post Max Size: ".$post_max_size;
    echo "Max Upload Size: ".$upload_max;
}
?>