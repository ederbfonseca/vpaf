<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Eder Fonseca" >

  <title>Visualizing Disjunctive Box Cluster Models of Fuzzy Tensors</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  
  <link rel="stylesheet" type="text/css" href="vendor/dropdown/css/jquery.dropdown.css">

  <!-- Custom styles for this template -->
  <link href="css/blog-post.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Visualizing Disjunctive Box Cluster Models of Fuzzy Tensors</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.html">Home
              <span class="sr-only">(current)</span>
            </a>

        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
		<div class="col-lg-3">
		<!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header">Settings</h5>
          <div class="card-body">
            <div class="row">
           <div class="col-md-12">
           <form>
           	<div class="form-group">
					<div class="dropdown-mul-1">
               	<select id="selectSearch" name="" multiple placeholder="Search">     </select>
          		</div>
          	</div>
          	<div class="form-group">
          		<!-- input id="searchAdv" type="button" class="btn btn-link btn-sm" value="Advanced Search" /-->
          	
          		<input id="searchReset" type="button" class="btn btn-outline-primary btn-sm" value="Clear" />
          	
          		<input id="searchButton" type="button" class="btn btn-outline-primary btn-sm" value="Search" />
				</div>
				</form>
				<form>
					<div class="form-group">
						<label id="labelDivSlider" for="divSlider">Number of Patterns</label>
						<div class="col-md-12" id="divSlider"></div>
						<label for="divSlider">Zoom</label>
        				<div class="col-md-12" id="divZoom"></div>
        			</div>
        		</form>
					          	
          	
        	  </div>
        	  <label for="leg_densidade">Densidade</label>
        	  <div class="col text-center" id="leg_densidade"></div>
          	<label for="leg_densidade">Area</label>          	          	
          	<div class="col text-center" id="leg_area"></div>
    		</div>
          </div>
        </div>

		</div>
      <!-- Post Content Column -->
      <div class="col-lg-7">

        <hr>
        <div class="row">
        <div class="col-md-1" id="divUpper"></div>
        
        </div>
        <div class="col-lg-8" id="vis-wrapper"></div>

        <!-- Preview Image -->
        <!-- img class="img-fluid rounded" src="http://placehold.it/900x620" alt="" -->

        <hr>
          

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-lg-2">
      
      	<!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header">Description</h5>
          <div class="card-body" style="min-height: 50px"  id="description">
           Pattern mouse over will show the pattern description. 
          </div>
        </div>

        
        

      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <!-- p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p -->
    </div>
    <!-- /.container -->
  </footer>
  <script type="text/javascript">
    <?php
  		$id = $_GET['id'];
  		echo 'var patternsFileName = "'.$id.'/exit.vis";';
  		echo 'var treeFileName = "'.$id.'/exit.tree";';
  		echo 'var matrixFileName = "'.$id.'/exit.matrix";';
  		//echo 'var fdata = "total.vis";';
  		?>
    </script>
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/dropdown/js/jquery.dropdown.js"></script>
  <script src="vendor/d3/d3.v5.min.js"></script>
  <script src="vendor/d3/d3-legend.min.js"></script>
  <script src="vendor/slider.js"></script>
  <script src="js/numeric.min.js"></script>
  <script src="js/mds.js"></script>
  <script src="js/fileReaders.js"></script>
  <script src="js/visualization.js"></script>

</body>

</html>
