var dag = [];
var matrix = [];
function arrayRemove(arr, value) {
	
   return arr.filter(function(ele){
       return ele != value;
   });

}
//Criando o array de arestas
	$.get(treeFileName, function(data) {
		dag = data.split("\n");
});
//criando a matriz de similaridade
$.get(matrixFileName, function(data) {
	var e = data.split("\n");
	e.pop();
	e.forEach(function (d) {
		matrix.push(d.split(" ").map(Number)); 
		//console.log(d.split(" "));
	});
	for (i = 0; i < matrix.length -1; i++) {
		for (j = i+1; j < matrix.length; j++) {
			matrix[i].push(matrix[j][i]);
		}
	}
	//console.log(matrix);
});

d3.text(patternsFileName).then(function(raw) {
	var dsv = d3.dsvFormat('\t');
	var data = dsv.parse(raw);
	var sizePatterns = data.length;
	//var root = Array.apply(null, {length: sizePatterns}).map(Number.call, Number)
	//console.log(root);
	visualization = new Visualization("#vis-wrapper", data);
	var e = visualization.generateDAGtoShow(data.length);
   //console.log(data.length);
   //console.log(this.calcMDS(e));
   var u = visualization.prepareVis(e, visualization.calcMDS(e, -1));
	visualization.updateGraph(u);
	visualization.createSearch();
	
});


