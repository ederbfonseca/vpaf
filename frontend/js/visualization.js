//DAG Full
//console.log(dag);
//console.log(matrix);

var Visualization = function (container, patterns) {
	var family = [];
	// DAG a ser exibida
	var edgesToShow = [];
	
	Visualization.prototype.matchConstraint = function (constraint, key){
		//console.log(patterns);
		var vectorMatch = [];
		patterns.forEach(function (p) {
			constraint.forEach(function (element) {
				//console.log(p.Pattern.toLowerCase().trim(),"=",element.toLowerCase().trim())
				if ((p.Pattern.toLowerCase().indexOf(element.toLowerCase()) == -1) && !vectorMatch.includes(p.Id)){
				//if ((p.Pattern.toLowerCase().trim() == element.toLowerCase().trim()) && !vectorMatch.includes(p.Id)){
					vectorMatch.push(p.Id);
				}
			});
		});
		return vectorMatch;
	}	
	
	Visualization.prototype.generateDAGtoShow = function (showing = -1, constraint = []) {
		// Showing = variavel do numero de padroes a serem exibidos (geralmente controlado pelo slider)
		//level = nivel que será exibido. Inicia com -1 (root) e após o clique exibe os filhos do pai clicado
		// por default showing é -1, ou seja, ira mostrar todos os padroes
		if (constraint !== []){
			var vectorMatch = visualization.matchConstraint(constraint);
			//console.log("vector",vectorMatch);
		}
		var root = Array.apply(null, {length: showing}).map(Number.call, Number);
		if (showing == -1){
			// showing == -1 significa que todos serão mostrados, ou seja, showing = patterns.length
			showing = patterns.length;
		} else {
			//percorre o vetor de arestas (DAG) removendo os padroes maiores que showing e atribuindo seus filhos aos avos
			//console.log(dag);
			reducedDAG = dag;
			var children = []
			dag.forEach(function (edge) {
				edge = edge.split(" ");
				//console.log(edge[0], patterns[edge[0]].Pattern);
				//array.includes(5)
				//console.log("c", constraint, "e", patterns[edge[0]].Pattern, "=",visualization.matchConstraint(constraint,edge[0]))
				if (edge[0] > showing || edge[1] > showing || vectorMatch.includes(edge[0]) || vectorMatch.includes(edge[1]) ){
					//console.log("remove",edge.join(" "));
					reducedDAG = arrayRemove(reducedDAG,edge.join(" "));
					if (edge[1] <= showing || !vectorMatch.includes(edge[1])) {
						children =+ edge.join(" ");
					}	
				}
			});
			//console.log("root", root);
			//console.log(reducedDAG, children);
			dag.forEach(function (edge) {
				for (child in children){
					if (child.split(" ")[1] == edge.split(" ")[0]){
						reduceDAG.push([child.split(" ")[0],edge.split(" ")[1]].join(" "));
					}
				}
			});
			
			var edges = [];
			var root = Array.apply(null, {length: showing}).map(Number.call, Number);
			vectorMatch.forEach(function (e) {
				root = arrayRemove(root, e);
			});
			reducedDAG.forEach(function (d) {
				if (!Array.isArray(edges[d.split(" ")[0]])){
					edges[d.split(" ")[0]] = [];
				}
				edges[d.split(" ")[0]].push(d.split(" ")[1]);
				root = arrayRemove(root, d.split(" ")[1]);
			});
			//console.log("root",root);
			edges[-1] = root.map(String);
			//console.log(edges);
			edgesToShow = edges;	
			return edges;
		}
	}
	
	Visualization.prototype.calcMDS = function (edges, father = "-1") {
		//father = 0;
		//console.log("f",father,"edges[father]",edges);
		var sm = [];
		
		var brothers = edges[father].map(Number);
		//console.log("edges",brothers.length);
		if(brothers.length <= 1){
			// quando há apenas um padrão, não precisa calcular MDS
			var t = [];
			t[0] = [0,0];
			return t;	
		} else {
			for (i in brothers){
				line = [];
				for (j in brothers){
					//console.log(brothers[i], brothers[j], matrix[brothers[i]][brothers[j]]);
					line.push(matrix[brothers[i]][brothers[j]]);
				}
				sm.push(line);
			}
			//console.log(sm);
			return mds.classic(sm);
		}
	}
	
	Visualization.prototype.prepareVis = function (edges, mds, level = -1) {
		
		if (level !== -1){
			d3.select("#labelDivSlider").style("visibility", "hidden");
			d3.select("#idSlider").style("visibility", "hidden");
			d3.select("#upper").style("visibility", "visible");
		} else {
			d3.select("#labelDivSlider").style("visibility", "visible");
			d3.select("#idSlider").style("visibility", "visible");
			d3.select("#upper").style("visibility", "hidden");
		}
		
		//visualization.prepareVis(edgesToShow, m, d.Id);
		//console.log("etos", edges, "m", mds, "d.id", level);
		var dataToShow = [];
		for (e in edges[level]){
			p = patterns[edges[level][e]];
			p.x = mds[e][0];
			p.y = mds[e][1];
			try {
				p.children = edges[edges[level][e]].length;
			} catch (err) {
				p.children = 0
			}
			dataToShow.push(p);
			//console.log(edges[level][e],mds[e], patterns[edges[level][e]]);
		}
		//console.log(mds);
		//console.log(dataToShow);
		return [dataToShow, edges];
	
	}
	
	//console.log(patterns);
   var width = 750;
	var height = 450;
	var margins = {top: 0, right: 10, bottom: 10, left: 10};
	function zoomed() {
            const currentTransform = d3.event.transform;
            svg.attr("transform", currentTransform);
            zSimple.property("value", currentTransform.k);
        }
   function slided(d) {
   	//console.log(svg);
   	zoom.scaleTo(svg, d);
   }
   var zoom = d3.zoom()
            .scaleExtent([1, 10])
            .on("zoom", zoomed);
   
	var svg = d3.select(container).append("svg")
    .attr("width", width)
    .attr("min-width", 400)
    .attr("height", height)
    .append("g")
    .call(zoom)
   
    //.call(drag);
	svg.attr("overflow","visible");
	//console.log(patterns.length)
	
	var upper = d3.select("#divUpper").append("input")
		.attr("type", "button")
		.attr("id", "upper")
		.attr("value", "Up")
		.attr("class", "btn btn-outline-primary btn-sm")
		.style("visibility", "hidden")
		.on("click", function click() {
			//console.log(family);
			var f = family.pop();
			//console.log(f);
			var m =  visualization.calcMDS(edgesToShow, family[family.length - 1]);
			var u = visualization.prepareVis(edgesToShow, m, family[family.length - 1]);
			visualization.updateGraph(u);
		});
	//Zoom
	var zoomSimple = d3
    .sliderHorizontal()
    .min(0)
    .max(100)
    .width(100)
    .tickFormat(d3.format('d'))
    .ticks(1)
    .default(0)
    .on("onchange", val => {
    	d3.select('#value').text(val)
    	slided(1+ (parseInt(val)/100))
    	});
	
	var zSimple = d3
    .select('div#divZoom')
    .append('svg')
    .attr('width', 150)
    .attr('height', 50)
    .attr("id", "idZoom")
    .append('g')
    .attr('transform', 'translate(20,10)');
   
   zSimple.call(zoomSimple);
	
	//Slider
	var sliderSimple = d3
    .sliderHorizontal()
    .min(1)
    .max(patterns.length)
    .width(200)
    .tickFormat(d3.format('d'))
    .ticks(10)
    .default(patterns.length)
    .on('end', val => {
    	d3.select('#value').text(val)
    	this.generateDAGtoShow(parseInt(val));
    	var c = this.calcMDS(edgesToShow)
    	var u = this.prepareVis(edgesToShow, c);
    	this.updateGraph(u);
    	dropdown.reset();
      //d3.select('p#value-simple').text(d3.format('.0%')(val));
    });
    
    var gSimple = d3
    .select('div#divSlider')
    .append('svg')
    .attr('width', 300)
    .attr('height', 50)
    .attr("id", "idSlider")
    .append('g')
    .attr('transform', 'translate(20,10)');
   
   gSimple.call(sliderSimple);
   //d3.select('p#value-simple').text(d3.format('.0%')(sliderSimple.value()));
	
	//var colors = [ "#2c7bb6", "#00a6ca", "#00ccbc", "#90eb9d", "#ffff8c","#f9d057","#f29e2e","#e76818","#d7191c"];
	var colors = [ "#ffffff", "#fff7ec", "#fee8c8", "#fdd49e", "#fdbb84", "#fc8d59", "#ef6548", "#d7301f", "#b30000", "#7f0000"];
   var colorsRange = d3.range(0, 1, 1.0 / (colors.length - 1));
   colorsRange.push(1);
   var colorScale = d3.scaleLinear()
   	.domain(colorsRange)
   	.range(colors)
   	.interpolate(d3.interpolateHcl);
   
   // Criando as legendas
   var legends_densidade = d3.select("#leg_densidade").append("svg")
  		.attr("width", 250)
  		.attr("height", 50);
  		
  	var defs = svg.append("defs");
	
	var linearGradient = defs.append("linearGradient")
    .attr("id", "linear-gradient");
    
   linearGradient.selectAll("stop") 
    .data( colors )                  
    .enter().append("stop")
    .attr("offset", function(d,i) { return i/(colors.length -1); })
    .attr("stop-color", function(d) { return d; });
    
   //Draw the rectangle and fill with gradient
	legends_densidade.append("rect")
		.attr("width", 200)
		.attr("height", 20)
		.attr("transform", "translate(" + (15) + "," + (5) + ")")
		.style("fill", "url(#linear-gradient)");
   
   var leg_xScale = d3.scaleLinear()
	 .range([0, 200])
	 .domain([0,1]);
	
	//Define x-axis
	var xAxis = d3.axisBottom(leg_xScale)
	  .ticks(5);
	
	legends_densidade.append("g")
	.attr("class", "axis")  //Assign "axis" class
	.attr("transform", "translate(" + (15) + "," + (25) + ")")
	.call(xAxis); 	
  
  	aMin = d3.min(patterns, function(d) { return parseFloat(d.Area); });
	aMax = d3.max(patterns, function(d) { return parseFloat(d.Area); });
	
	/*
	function areaScale(a) {
		//return 400 * Math.sqrt(Math.log(1 + a));
		return a;
	}
	*/
	
	var areaScale = d3.scaleLinear()
	.domain([aMin,aMax]) //variacao da area minima a area maxima
	.range([400,20000]); //sera exibida dentro dessas medidas
	
	rMin = Math.sqrt(areaScale(aMin)/Math.PI);
	rMax = Math.sqrt(areaScale(aMax)/Math.PI);
	
	var linearSize = d3.scaleLinear().domain([aMin, aMax]).range([rMin, rMax]);
	
	
	var l_svg = d3.select("#leg_area")
		.append("svg")
		.attr("width", 250)
  		.attr("height", 200);
  	// The scale you use for bubble size
	var size = d3.scaleSqrt()
  		.domain([1, 100])  // What's in the data, let's say it is percentage
  		.range([1, 10])  // Size in pixel
  	//var valuesToShow = [rMin, (rMax+rMin)/2, rMax]
  	var valuesToShow = [10, 100, 1000, 10000]
  	//console.log(valuesToShow);
  	var xCircle = 100;
	var xLabel = 220;
	var yCircle = 200
	
	l_svg
  .selectAll("legend")
  .data(valuesToShow)
  .enter()
  .append("circle")
    .attr("cx", xCircle)
    .attr("cy", function(d){ return yCircle - size(d) } )
    .attr("r", function(d){ return size(d) })
    .style("fill", "none")
    .attr("stroke", "black");
   
   // Add legend: segments
	l_svg
  .selectAll("legend")
  .data(valuesToShow)
  .enter()
  .append("line")
    .attr('x1', function(d){ return xCircle + size(d) } )
    .attr('x2', xLabel)
    .attr('y1', function(d){ return yCircle - size(d) } )
    .attr('y2', function(d){ return yCircle - size(d) } )
    .attr('stroke', 'black')
    .style('stroke-dasharray', ('2,2'))
   
   // Add legend: labels
l_svg
  .selectAll("legend")
  .data(valuesToShow)
  .enter()
  .append("text")
    .attr('x', xLabel)
    .attr('y', function(d){ return yCircle - size(d) } )
    .text( function(d){ return d } )
    .style("font-size", 10)
    .attr('alignment-baseline', 'middle')

	/*
	//var l_svg = d3.select("#leg_area svg");
	var l_svg = d3.select("#leg_area").append("svg")
		.attr("width", 550)
  		.attr("height", 280);

	l_svg.append("g")
	.attr("class", "legendSize")
	.attr("transform", "translate(20, "+rMax+")");
	
	
	var legendSize = d3.legendSize()
  		.scale(linearSize)
  		.shape('circle')
  		.shapePadding(25)
  		.labelOffset(20)
  		.labelFormat(d3.format(".0f"))
  		.orient('horizontal');

		l_svg.select(".legendSize")
  		.call(legendSize)
  		.style("fill", "#000fff");
  		*/
   // Inicializando o menu dropdown de pesquisa vazio.  
	var dropdown = $('.dropdown-mul-1	').dropdown({
			limitCount: 40,
			multipleMode: 'label',
			data: [],
			choice: function () {
				
				//Visualization.doSearch(sch);
			}
	}).data('dropdown');
	
	searchButton = d3.select('#searchButton');
	searchButton
		.on("click", function () {
			var sch = [];
			var query = $('.dropdown-selected');
			for (var i = 0; i < query.length; i++){
				sch.push(query[i].innerText);
			}
			//console.log(sliderSimple.value());
			//console.log("sch",sch);
			visualization.generateDAGtoShow(sliderSimple.value(), sch);
			var c = visualization.calcMDS(edgesToShow);
			var u = visualization.prepareVis(edgesToShow, c);
			visualization.updateGraph(u);
		});
	searchResetButton = d3.select('#searchReset');
	searchResetButton
		.on("click",function () {
			dropdown.reset();
			var sch = [];
			visualization.generateDAGtoShow(sliderSimple.value(), sch);
			var c = visualization.calcMDS(edgesToShow);
			var u = visualization.prepareVis(edgesToShow, c);
			visualization.updateGraph(u);
		});
   	
	Visualization.prototype.createSearch = function () {
		
		//console.log(patterns);
		var uniquePattern = new Array();
		var select = d3.select('#selectSearch');
		patterns.forEach( function (d) {
			for (var i = 0; i < d.Pattern.split(" ").length; i++){
				// Se ainda nao foi iniciado, inicia o array da dimensao i
				if (typeof uniquePattern[i] == "undefined"){
					uniquePattern[i] = [];
				}
				// concatena o array existente com o novo array
				uniquePattern[i] = uniquePattern[i].concat(d.Pattern.split(" ")[i].split(","));
				// remove as repeticoes no array da posicao i
				uniquePattern[i] = uniquePattern[i].filter((v, p) => uniquePattern[i].indexOf(v) == p);
			}
		
		});
		//console.log(uniquePattern);
		var dict = [];
		var i = 0
		uniquePattern.forEach(function (d, id) {
			d.forEach(function (e) {
				dict.push({
					"id": i,
					"disable": false,
					"groupName": "Dimension "+(parseInt(id)+1),
					"groupId": id+1,
					"selected": false,
					"name": e,
				});
				i++;
			});
		});
		// Faz o update do menu de busca
		dropdown.update(dict,true);
	}
	Visualization.prototype.intersections = function (patterns, p) {
		var dist = [];
		patterns.forEach(function (e) {
			var elements = e.Pattern.split(" ");
			//console.log('Pattern',p);
			//console.log(elements);
			var percent = 1;
			for (var i=0; i<elements.length;i++){
				var a = elements[i].split(',');
				//console.log("a",a)
				var b = p[i].split(',');
				//console.log("b",b)
				var c = $(b).not($(b).not(a));
				//console.log("c",c)
				if (a.length != 0){
					//console.log(c, b, c.length/b.length);
					percent = percent * (c.length/a.length);
					//percent = percent * c.length;
				}
				
			}
			dist.push(percent);
			//console.log('prcent', percent);
			//console.log( p.Pattern.split(" ")); 
		});
		return dist
	}
	
	Visualization.prototype.updateGraph = function (dataToShow){
		//console.log(dataToShow);
		
		svg.selectAll("circle").remove();
		svg.selectAll("text").remove();
		
		xMin = d3.min(dataToShow[0], function(d) { return parseFloat(d.x); });
		xMax = d3.max(dataToShow[0], function(d) { return parseFloat(d.x); });
		yMin = d3.min(dataToShow[0], function(d) { return parseFloat(d.y); });
		yMax = d3.max(dataToShow[0], function(d) { return parseFloat(d.y); });
		aMin = d3.min(dataToShow[0], function(d) { return parseFloat(d.Area); });
		aMax = d3.max(dataToShow[0], function(d) { return parseFloat(d.Area); });
		rMin = Math.sqrt((areaScale(aMin))/Math.PI);
		rMax = Math.sqrt((areaScale(aMax))/Math.PI);
		//console.log(xMin,xMax,yMin,yMax);
		
		Xscale = d3.scaleLinear()
			.domain([xMin,xMax])
			.range([0+rMax,width-rMax]);
		
		//Scale
		Yscale = d3.scaleLinear()
			.domain([yMin,yMax])
			.range([0+rMax,height-rMax]);
	
		
		var circles = svg.selectAll('circle')
		.data(dataToShow[0]);
		
		circles.enter().append('circle')
		.attr("r", function(d) {  return Math.sqrt((areaScale(parseFloat(d.Area)))/Math.PI); })
		.attr("cx", function (d) { return Xscale(d.x); })
		.attr("cy", function (d) { return Yscale(d.y);})
		.style("fill", function(d) { return colorScale(d.Density); })
		.on("click", function (d) {
				if ( d.children > 0 ) {
					family.push(d.Id);
					//console.log(edgesToShow, d.Id);
					var m =  visualization.calcMDS(edgesToShow, d.Id);
					var u = visualization.prepareVis(edgesToShow, m, d.Id);
					visualization.updateGraph(u);
				}
		 })
		 .on("mouseover", function (d) {
		 	var distances = visualization.intersections(dataToShow[0], d.Pattern.split(" "));
		 	//console.log("id", d.Id);
		 	//console.log(distances);
		 	//var pieData = {}
		 	dataToShow[0].forEach(function (element,i) {
		 		
		 		var pieData = {a:100*distances[i], b:100*(1-distances[i])}
		 		var color = d3.scaleOrdinal()
		 			.domain(pieData)
		 			.range(['#000000','#E4959E']);
		 		var radius = 50;
		 		var pie = d3.pie()
		 			.value(function(e) {return e.value; })
		 		var data_ready = pie(d3.entries(pieData))
		 		svg
		 			.selectAll('whatever')
		 			.data(data_ready)
		 			.enter()
		 			.append('path')
		 			.attr('d', d3.arc()
		 				.innerRadius(0)
		 				.outerRadius(Math.sqrt((areaScale(parseFloat(element.Area)))/Math.PI)))
		 			.attr('fill', function(e){ 
		 				if (e.data.key == 'a') { 
		 					return(color(e.data.key));
		 				} else {
		 					return colorScale(element.Density);
		 				}
		 			 })
		 			.attr("stroke", "black")
		 			.attr('transform', function (e) { return "translate("+Xscale(element.x)+","+Yscale(element.y)+")"; })
		 			.style("stroke-width", "0px")
		 			.style("opacity", 0.9)
		 			.on("click", function (e) {
						if ( d.children > 0 ) {
						svg.selectAll('path').remove();
						family.push(d.Id);
						//console.log(edgesToShow, d.Id);
						var m =  visualization.calcMDS(edgesToShow, d.Id);
						var u = visualization.prepareVis(edgesToShow, m, d.Id);
						visualization.updateGraph(u);
					}
					})
		 			.on("mouseout", function () {
		 				svg.selectAll('path').remove();
		 			});
		 		//console.log(element);
		 		//console.log(pieData);
		 	});
		 	
		 	d3.select("#description").html(d.Pattern.split(" ").join("<br><br>"))
		 })
		 
		
		circles.exit().remove();
		
		var text = svg.selectAll('text')
			.data(dataToShow[0]);
		
		text.enter()
			.append("text")
			.attr("class", "textos")
			.attr("x", function (d) { return Xscale(d.x) - 2.5; })
			.attr("y", function (d) { return Yscale(d.y) + 2.5; })
			.text(function (d) { if ( d.children > 0 ) { return Math.floor(d.children); }  })
			.on("mouseover", function (d) { /* console.log(d); */ })
			.on("click", function (d) {
				if ( d.children > 0 ) {
					family.push(d.Id);
					//console.log(edgesToShow, d.Id);
					var m =  visualization.calcMDS(edgesToShow, d.Id);
					var u = visualization.prepareVis(edgesToShow, m, d.Id);
					visualization.updateGraph(u);
				}
			});
		text.exit().remove();
	}
	
	
}
