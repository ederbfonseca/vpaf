<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Eder Fonseca" >

    <title>Visualizing Disjunctive Box Cluster Models of Fuzzy Tensors</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->

<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>

  </head>
  <?php
  
  //print_r($_POST);
  $unprefixed_keys = preg_filter('/^dim(.*)/', '$1', array_keys( $_POST ));
  $dir = $_POST['dir'];
  $patterns = $_POST['patterns'];
  $matrix = $_POST['matrix'];
  $data = array("dir" => $dir,"patterns" => $patterns,"matrix" => $matrix);


 ?>
  <body >
  
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.html">Visualizing Disjunctive Box Cluster Models of Fuzzy Tensors</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.html">Home
              </a>
            </li>
           
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Heading Row -->
      <div class="row my-4">
        <!-- div class="col-lg-4">
        <!--p>You can upload a file here.</p>
        	<form enctype="multipart/form-data" method="post" id="fileinfo">
        	<label>Select a file:</label>
        	
        	<input type="file" name="file" required />
        	
        	<input type="button" value="Upload" onclick="uploadFile();" />
        	</form>
           <div id="output" class="alert alert-success" hidden="hidden" ></div>
			>
        </div -->
        <!-- /.col-lg-8 -->
        <div class="col-lg-12">
          <h4>Execution Process</h4>
          <p></p>
          <div id="execution" class="row" style="min-height: 250px">
         	<div class="col-sm-12">
         	<?php
         	// tell php to automatically flush after every output
         	// including lines of output produced by shell commands
         	//disable_ob();
         	//$command = 'ping 127.0.0.1';
         	//system($command);
         	
         	?>
				<iframe src='teste2.php?data=<?php echo json_encode($data); ?>' style="border:none;width:100%;height:100%;" scrolling="no" onload="resizeIframe(this)"></iframe>
         	</div>
         	<div id="link" class="col-sm-12">
         		
         	</div>
          </div>
        </div>
        <!-- /.col-md-4 -->
      </div>
      <!-- /.row -->

      <!-- Call to Action Well -->
      <div class="card text-white bg-secondary my-4 text-center">
        <div class="card-body">
          <!--p class="text-white m-0">This call to action card is a great place to showcase some important information or display a clever tagline!</p-->
        </div>
      </div>

      

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white"></p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
