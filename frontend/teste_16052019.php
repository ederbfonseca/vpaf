<?php


function disable_ob() {
    // Turn off output buffering
    ini_set('output_buffering', 'off');
    // Turn off PHP output compression
    ini_set('zlib.output_compression', false);
    // Implicitly flush the buffer(s)
    ini_set('implicit_flush', true);
    ob_implicit_flush(true);
    // Clear, and turn off output buffering
    while (ob_get_level() > 0) {
        // Get the curent level
        $level = ob_get_level();
        // End the buffering
        ob_end_clean();
        // If the current level has not changed, abort
        if (ob_get_level() == $level) break;
    }
    // Disable apache output buffering/compression
    if (function_exists('apache_setenv')) {
        apache_setenv('no-gzip', '1');
        apache_setenv('dont-vary', '1');
    }
}
// tell php to automatically flush after every output
// including lines of output produced by shell commands
disable_ob();
$data = json_decode($_GET["data"]);
$data = get_object_vars($data);
$upload_dir = "upload/".$data["dir"];

echo "<pre>";


$command = "../bin/multidupehack -e '".implode(" ", $data["epsilon"])."' -s '".implode(" ", $data["dims"])."' ".$upload_dir.$data["filename"]." -o ".$upload_dir."exit.multidupehack";
//echo $command;
//$command = "../bin/multidupehack -e '0.5 0.25 1' -s '4 8 2' ".$upload_dir.$data["filename"]." -o ".$upload_dir."exit.multidupehack";
echo "Executing multidupehack...\n";
echo system($command);
$command = "cat ".$upload_dir."exit.multidupehack | ../bin/paf --pa --sas ' ' -vf ".$upload_dir.$data["filename"]." -a 10000 -o ".$upload_dir."exit.paf";
echo system($command);
$command = "mv matrix.txt ".$upload_dir."exit.matrix";
echo system($command);

$command = "python ../bin/visualization.py ".$upload_dir."exit.paf ".$upload_dir."exit.tree ".$upload_dir."exit.vis";
echo system($command);

#$command = "python ../bin/mds.py ".$upload_dir."exit.paf ".$upload_dir."exit.tree ".$upload_dir."exit.matrix ".$upload_dir."total.vis";
//echo $command;
#echo system($command);

echo "</pre>";


?>

<a href=" <?php echo "http://localhost/frontend/visualization.php?id=".$upload_dir; ?>" target="_blank" > Go to Visualization </a>