Download the source code.

First, compile multidupehack and paf source code:
```shell
cd bin/multidupehack

make

cd bin/paf

make

```

After this, in the root path, run:

```shell
sudo php -S localhost:80
```
Then open the address 'localhost' in Chrome.

Requirements: php-cli
